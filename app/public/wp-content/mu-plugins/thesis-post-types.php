<?php 
function thesis_post_types(){

	//Campus post type
	register_post_type('campus', array(
		'supports' => array('title','editor','excerpt'),
		'rewrite' => array('slug' => 'campuses'),
		'has_archive' => true,
		'public' => true,
		'labels' => array(
			'name' => 'Campuses',
			'add_new_item' => 'Add New Campus',
			'edit_item' => "Edit Campus",
			'all_items' => 'All Campuses',
			'singular_name' => 'Campus' 
		),
		'menu_icon' => 'dashicons-location-alt'
	));

	//event post type
	register_post_type('event', array(
		'capability_type' => 'event',
		'map_meta_cap' => true,
		'supports' => array('title','editor','excerpt'),
		'rewrite' => array('slug' => 'events'),
		'has_archive' => true,
		'public' => true,
		'labels' => array(
			'name' => 'Events',
			'add_new_item' => 'Add New Event',
			'edit_item' => "Edit Event",
			'all_items' => 'All Events',
			'singular_name' => 'Event' 
		),
		'menu_icon' => 'dashicons-calendar'
	));

	//course post type
	register_post_type('course', array(
		'capability_type' => 'course',
		'map_meta_cap' => true,
		'supports' => array('title'),
		'rewrite' => array('slug' => 'courses'),
		'has_archive' => true,
		'public' => true,
		'labels' => array(
			'name' => 'Courses',
			'add_new_item' => 'Add New Course',
			'edit_item' => "Edit Course",
			'all_items' => 'All Courses',
			'singular_name' => 'Course' 
		),
		'menu_icon' => 'dashicons-book'
	));

	//professor post type
	register_post_type('professor', array(
		'capability_type' => 'professor',
		'map_meta_cap' => true,
		'show_in_rest' => true,
		'supports' => array('title','editor','thumbnail'),
		'public' => true,
		'labels' => array(
			'name' => 'Professors',
			'add_new_item' => 'Add New Professor',
			'edit_item' => "Edit Professor",
			'all_items' => 'All Professors',
			'singular_name' => 'Professor' 
		),
		'menu_icon' => 'dashicons-businessman'
	));

	//Student post type
	register_post_type('student', array(
		'capability_type' => 'student',
		'map_meta_cap' => true,
		'show_in_rest' => true,
		'supports' => array('title','editor','thumbnail'),
		'public' => true,
		'labels' => array(
			'name' => 'Students',
			'add_new_item' => 'Add New Student',
			'edit_item' => "Edit Student",
			'all_items' => 'All Students',
			'singular_name' => 'Student' 
		),
		'menu_icon' => 'dashicons-universal-access'
	));

	//notes post type
	register_post_type('note', array(
		'capability_type' => 'note',
		'map_meta_cap' => true,
		'show_in_rest' => true,
		'supports' => array('title','editor','comments'),
		'public' => false,
		'show_ui' => true,
		'labels' => array(
			'name' => 'Notes',
			'add_new_item' => 'Add New Note',
			'edit_item' => "Edit Note",
			'all_items' => 'All Notes',
			'singular_name' => 'Note' 
		),
		'menu_icon' => 'dashicons-welcome-write-blog'
	));

	//notes post type
	register_post_type('like', array(
		'supports' => array('title'),
		'public' => false,
		'show_ui' => true,
		'labels' => array(
			'name' => 'Likes',
			'add_new_item' => 'Add New Like',
			'edit_item' => "Edit Like",
			'all_items' => 'All Likes',
			'singular_name' => 'Like' 
		),
		'menu_icon' => 'dashicons-heart'
	));

	
}

add_action('init','thesis_post_types');
