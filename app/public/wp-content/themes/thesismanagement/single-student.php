<?php get_header();

while(have_posts()){
  the_post(); 
  pageBanner();
  ?>

<div class="container container--narrow page-section">
  <div class="metabox metabox--position-up metabox--with-home-link">
      
    </div>
  <div class="generic-content">
   <div class="row group">
     <div class="one-third">
      <?php the_post_thumbnail('professorPortrait'); ?>

     </div>
     <div class="two-thirds">
      <?php

        $likeCount = new WP_Query(array(
          'post_type' => 'like',
          'meta_query' => array(
            array(
              'key' => 'like_professor_id',
              'compare' => '=',
              'value' => get_the_ID()
            )
          )

        ));

        $existStatus = 'no';

        if (is_user_logged_in()) {
          $existQuery = new WP_Query(array(
          'author' => get_current_user_id(),
          'post_type' => 'like',
          'meta_query' => array(
            array(
              'key' => 'like_professor_id',
              'compare' => '=',
              'value' => get_the_ID()
            )
          )
        ));

        if ($existQuery->found_posts) {
          $existStatus = 'yes';
        }
        }
        

          the_content();
         ?>

       </div>
   </div>


  </div>

  <?php 

    $relatedCourses = get_field('related_courses');

    if ($relatedCourses) {
      echo '<hr class="section-break">';
    echo '<h2 class="headline headline--medium">Course(s) Enrolled</h2>';
    echo '<ul class="link-list min-list">';
    foreach ($relatedCourses as $course) {
      ?>
        <li><a href="<?php echo get_the_permalink($course); ?>"><?php echo get_the_title($course); ?></a></li>

      <?php
    }

    echo '</ul>';
    }
    

  ?>
</div>


<?php }

get_footer();
?>

