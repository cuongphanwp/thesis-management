<?php
get_header(); 
pageBanner(array(
  'title' => 'All Events',
  'subtitle' => 'See what is going to happen in the following period of time'
))
?>
 

<div class="container container--narrow page-section">
<?php

while (have_posts()) {
	the_post(); 
  get_template_part('template_parts/content-event');	
}

echo paginate_links();
?>	
<hr class="section-break">
<p>Looking for a recap of past events? <a href="<?php echo site_url('/past-events'); ?>">Check out here.</a></p>
</div>




<?php
get_footer();
?>