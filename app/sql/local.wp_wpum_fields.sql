/*!40101 SET NAMES binary*/;
/*!40014 SET FOREIGN_KEY_CHECKS=0*/;
/*!40103 SET TIME_ZONE='+00:00' */;
INSERT INTO `wp_wpum_fields` VALUES
(1,1,"username","Username","",0,1,1,0,"public","disallowed","","username"),
(2,1,"email","Email","",0,1,1,0,"public","disallowed","","user_email"),
(3,1,"password","Password","",0,1,1,0,"public","disallowed","","password"),
(4,1,"text","First Name","",0,0,0,0,"public","disallowed","","first_name"),
(5,1,"text","Last Name","",0,0,0,0,"public","disallowed","","last_name"),
(6,1,"nickname","Nickname","",0,1,0,0,"public","disallowed","","nickname"),
(7,1,"display_name","Display Name","",0,1,0,0,"public","disallowed","","display_name"),
(8,1,"text","Website","",0,0,0,0,"public","disallowed","","user_url"),
(9,1,"textarea","Description","",0,0,0,0,"public","disallowed","","description"),
(10,1,"avatar","Profile Picture","",0,0,0,0,"public","disallowed","","user_avatar");
